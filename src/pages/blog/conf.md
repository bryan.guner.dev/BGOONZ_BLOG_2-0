---
title: What I am doing imediatly after the pandemic
excerpt: >-
  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
  incididunt ut labore et dolore magna aliqua. Ac ut consequat semper viverra
  nam libero justo laoreet sit.
date: '2019-03-10'
thumb_image: images/giphy.gif
image: images/2.jpg
seo:
  title: New Conference
  description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
  extra:
    - name: 'og:type'
      value: article
      keyName: property
    - name: 'og:title'
      value: New Conference
      keyName: property
    - name: 'og:description'
      value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
      keyName: property
    - name: 'og:image'
      value: images/2.jpg
      keyName: property
      relativeUrl: true
    - name: 'twitter:card'
      value: summary_large_image
    - name: 'twitter:title'
      value: New Conference
    - name: 'twitter:description'
      value: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit'
    - name: 'twitter:image'
      value: images/2.jpg
      relativeUrl: true
template: post
---


>Creativity is allowing yourself to make mistakes. Design is knowing which ones to keep. - Scott Adams


  <button class="js-gitter-toggle-chat-button">Toggle Chat</button>
  <button class="js-gitter-toggle-chat-button" data-gitter-toggle-chat-state="true">Open Chat</button>
  <button class="js-gitter-toggle-chat-button" data-gitter-toggle-chat-state="false">Close Chat</button>
  <script>
    ((window.gitter = {}).chat = {}).options = {
      room: 'gitterHQ/sidecar-demo',
      activationElement: false
    };
  </script>
  <script src="../../dist/sidecar.js" async defer></script>
