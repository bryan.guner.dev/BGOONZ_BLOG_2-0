---
title: Contact
weight: 1.5
seo:
  title: Installation
  description: This is the installation page
  extra:
    - name: 'og:type'
      value: website
      keyName: property
    - name: 'og:title'
      value: Installation
      keyName: property
    - name: 'og:description'
      value: This is the installation page
      keyName: property
    - name: 'twitter:card'
      value: summary
    - name: 'twitter:title'
      value: Installation
    - name: 'twitter:description'
      value: This is the installation page
template: docs
excerpt: Portfolio Resume PDF Bryan's email Blog Linkedin AngelList GitHub bgoonz
---
➤ \*Email                          \*

## &#xA;[](https://github.com/bgoonz#bryangunergmailcom)[bryan.guner@gmail.com](https://github.com/bgoonz#)

[![](https://raw.githubusercontent.com/andreasbm/readme/master/assets/lines/colored.png)](https://github.com/bgoonz#\_phone\_)

## [](https://github.com/bgoonz#-phone)➤ *Phone*

\*\*

#### [](https://github.com/bgoonz#551-254-5505)[551-254-5505](https://github.com/bgoonz/bgoonz/blob/master/551-254-5505)

## [](https://github.com/bgoonz#-connect-with-me)➤ Connect with me:

[![](https://camo.githubusercontent.com/987e9d7d7e70f98c5cd1d613add6ef56bcab60a8eda9e3df9f4f505b611ff773/68747470733a2f2f696d672e69636f6e73382e636f6d2f636f6c6f722f39362f3030303030302f676d61696c2e706e67)](mailto:bryan.guner@gmail.com)[![](https://camo.githubusercontent.com/6acccefe72a9ad3380c0802e7a78988adad9d186eefff43b715bd7d7d07dc52a/68747470733a2f2f696d672e69636f6e73382e636f6d2f636f6c6f722f39362f3030303030302f66616365626f6f6b2e706e67)](https://www.facebook.com/bryan.guner/)[![](https://camo.githubusercontent.com/a6a76173a04df8b3239debac70858c0d8bebd8d882b6572a0419cff3f51a2fc3/68747470733a2f2f696d672e69636f6e73382e636f6d2f636f6c6f722f39362f3030303030302f747769747465722d737175617265642e706e67)](https://twitter.com/bgooonz)[![](https://camo.githubusercontent.com/aec347ccecfb57c504334b6723d26a419c1e7a871d467603d0a301272d5ac329/68747470733a2f2f696d672e69636f6e73382e636f6d2f636f6c6f722f39362f3030303030302f796f75747562652e706e67)](https://www.youtube.com/channel/UC9-rYyUMsnEBK8G8fCyrXXA/videos)[![](https://camo.githubusercontent.com/13b4ab64e1a639ef039c1688b03c7a1a0aaa875a1858fa56888aa09c492aac6a/68747470733a2f2f696d672e69636f6e73382e636f6d2f636f6c6f722f39362f3030303030302f696e7374616772616d2d6e65772e706e67)](https://www.instagram.com/bgoonz/?hl=en)[![](https://camo.githubusercontent.com/4d14fb643e9f849728e8157811f502e1f146b7a2766a4c0a90e22afe106c0fbd/68747470733a2f2f696d672e69636f6e73382e636f6d2f636f6c6f722f39362f3030303030302f70696e7465726573742d2d76312e706e67)](https://www.pinterest.com/bryanguner/\_saved/)[![](https://camo.githubusercontent.com/4f660401d8469647f004f5740254c81a657f48d4c55a635be05ffb196c2be320/68747470733a2f2f696d672e69636f6e73382e636f6d2f636f6c6f722f39362f3030303030302f6c696e6b6564696e2e706e67)](https://www.linkedin.com/in/bryan-guner-046199128/) 

*   [GitHub](https://github.com/bgoonz)

*   [Gitlab](https://gitlab.com/bryan.guner.dev)

*   [Bitbucket](https://bitbucket.org/bgoonz/)

*   [code pen](https://codepen.io/bgoonz)

*   [Glitch](https://glitch.com/@bgoonz)

*   [Replit](https://repl.it/@bgoonz/)

*   [Redit](https://www.reddit.com/user/bgoonz1)

*   [runkit](https://runkit.com/bgoonz)

*   [stack-exchange](https://meta.stackexchange.com/users/936785/bryan-guner)

*   [Netlify](https://app.netlify.com/user/settings#profile)

*   [Medium](https://bryanguner.medium.com/)

*   [webcomponents.dev](https://webcomponents.dev/user/bgoonz)

*   [npm](https://www.npmjs.com/~bgoonz11)

*   [Upwork](https://www.upwork.com/freelancers/~01bb1a3627e1e9c630?viewMode=1\&s=1110580755057594368)

*   [AngelList](https://angel.co/u/bryan-guner)

*   [Quora](https://www.quora.com/q/webdevresourcehub?invite_code=qwZOqbpAhgQ6hjjGl8NN)

*   [dev.to](https://dev.to/bgoonz)

*   [Observable Notebooks](https://observablehq.com/@bgoonz?tab=profile)

*   [Notation](https://www.notion.so/Overview-Of-Css-5d88b0bc9a73422a9be1481d599a56ba)

*   [StackShare](https://stackshare.io/bryanguner)

*   [Plunk](http://plnkr.co/account/plunks)

*   [Dribble](https://dribbble.com/bgoonz4242?onboarding=true)

*   [contentful](https://app.contentful.com/spaces/lelpu0ihaz11/assets?id=MocOPmmNliLn6PPv)

*   [giphy](https://giphy.com/channel/bryanguner)



