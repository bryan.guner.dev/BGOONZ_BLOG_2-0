---
title: Bryan Guner Web Dev Resource Gub
sections:
  - section_id: hero
    type: section_hero
    title: I am a musician/electrical engineer turned web developer
    image: images/code.gif
    content: >-
      This section can contain a subtitle or tagline. The recommended length is
      one to three sentences, but can be changed as you prefer.
    actions: []
  - section_id: features
    type: section_grid
    col_number: three
    grid_items:
      - title: Data Structures Series
        content: >
          ###### **A Quick Guide to Big-O Notation, Memoization, Tabulation, and
          Sorting**


          ###### ![](https://miro.medium.com/max/2000/0\*yjlSk3T9c2\_14in1.png)


          ***Curating Complexity: A Guide to Big-O Notation***
        actions:
          - label: Tell me about data structures
            url: /docs
            style: link
      - title: Mini Project Showcase
        content: >
          ###### ***Whenever I find my skills laking I try to learn by doing a
          mini project that emphasizes the skill I want to improve on!***


          ******![](/images/important-tiger.png)
        actions:
          - label: Navigate to showcase
            url: 'https://project-showcase-bgoonz.netlify.app/'
            style: link
      - title: Long Term Asperations
        content: >
          ***I want to make things that change the lives of the people that use
          them for the better.***
        actions:
          - label: My goals and how I plan to achieve them
            url: >-
              #https://github.com/bgoonz/Revamped-Automatic-Guitar-Effect-Triggering
            style: link
            icon_class: dev
            new_window: false
            no_follow: false
            type: action
        image: images/goals.jpg
      - title: lorem-ipsum
        title_url: lorem-ipsum
        image_alt: lorem-ipsum
        content: >-
          ## Lorem ipsum


          Lorem ipsum dolor sit amet, **consectetur adipiscing elit**, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.


          - Lorem ipsum

          - dolor sit amet
        actions: []
        type: grid_item
      - title: lorem-ipsum
        title_url: lorem-ipsum
        image_alt: lorem-ipsum
        content: >-
          ## Lorem ipsum


          Lorem ipsum dolor sit amet, **consectetur adipiscing elit**, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.


          - Lorem ipsum

          - dolor sit amet
        actions: []
        type: grid_item
      - title: lorem-ipsum
        title_url: lorem-ipsum
        image_alt: lorem-ipsum
        content: >-
          ## Lorem ipsum


          Lorem ipsum dolor sit amet, **consectetur adipiscing elit**, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.


          - Lorem ipsum

          - dolor sit amet
        actions: []
        type: grid_item
  - section_id: text-img
    type: section_content
    image_position: left
    title: 'Music, Engineering & Lifelong Pursuit of Knowledge'
    content: |
      https://www.youtube.com/watch?v=xGZSWvFess8
    actions: []
  - section_id: features-two-col
    type: section_grid
    title: Resume & Portfolio
    subtitle: An optional subtitle of the section
    col_number: two
    grid_items:
      - title: Overview
        content: |
          ![](/images/skillz.PNG)
        actions:
          - label: Learn More
            url: /overview
            style: link
      - title: Showcase
        content: |
          Here's a series of mini projects I work on to sharpen my skills!
        actions:
          - label: Learn More
            url: /showcase
            style: link
  - section_id: cta
    type: section_cta
    title: Contact Me!
    subtitle: This is an optional description for the call to action block.
    actions:
      - label: Don't be shy
        url: /docs/getting-started/installation
        style: primary
  - title: lorem-ipsum
    section_id: lorem-ipsum
    image_alt: lorem-ipsum
    image_position: left
    content: >-
      ## Lorem ipsum


      Lorem ipsum dolor sit amet, **consectetur adipiscing elit**, sed do
      eiusmod tempor incididunt ut labore et dolore magna aliqua.


      - Lorem ipsum

      - dolor sit amet
    actions: []
    type: section_content
  - title: lorem-ipsum
    section_id: lorem-ipsum
    subtitle: lorem-ipsum
    col_number: three
    type: section_docs
seo:
  title: Stackbit Libris Theme
  description: The preview of the Libris theme
  extra:
    - name: 'og:type'
      value: website
      keyName: property
    - name: 'og:title'
      value: Stackbit Libris Theme
      keyName: property
    - name: 'og:description'
      value: The preview of the Libris theme
      keyName: property
    - name: 'og:image'
      value: images/4.jpg
      keyName: property
      relativeUrl: true
    - name: 'twitter:card'
      value: summary_large_image
    - name: 'twitter:title'
      value: Stackbit Libris Theme
    - name: 'twitter:description'
      value: The preview of the Libris theme
    - name: 'twitter:image'
      value: images/4.jpg
      relativeUrl: true
template: advanced
---
